#ifndef FEMOBJECT_H
#define FEMOBJECT_H

#include "node.h"
#include "scenario.h"

class FEMObject : public GMlib::TriangleFacets<float>
{
public:
    FEMObject();
    ~FEMObject() override;

    void square(int n, int m, float w, float h);
    void regularTriangulation(int n, int m, float r);
    void randomTriangulation(int r, int m);
    void computation();

protected:
    virtual void localSimulate(double dt) override;

private:
    GMlib::Vector<GMlib::Vector<float, 2>, 3> _findVectors(GMlib::TSEdge<float>* ed);
    GMlib::Vector<GMlib::Vector<float, 2>, 3> _findVectors(GMlib::TSTriangle<float>* tr, Node* n);
    void _createRegularVertices(int n, int m, float r);
    float _computeElement(GMlib::TSEdge<float>* ed);
    float _computeElement(Node* n);
    void _updateHeight(float);

    GMlib::ArrayLX<Node> _nodes; // Internal nodes
    GMlib::DMatrix<float> _A; // stiffness matrix
    GMlib::DVector<float> _b; // load vector
    GMlib::DVector<float> _x; // Vector of weights
    int _boundaryPoints;
    float _f = 0.0f; // force
};

#endif // FEMOBJECT_H
