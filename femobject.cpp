#include "femobject.h"
#include <cmath>
#include <random>

using namespace GMlib;

FEMObject::FEMObject()
{
}

FEMObject::~FEMObject() {

}

void FEMObject::square(int n, int m, float w, float h)
{
    for(int i = 0; i < n; ++i) {
        for(int j = 0; j < m; ++j) {
            float x = i * w / n;
            float y = j * h / m;
            this->insertAlways(GMlib::TSVertex<float>(GMlib::Point<float, 2>(x, y)));
        }
    }

    this->triangulateDelaunay();

    for(int i = 0; i < this->getNoVertices(); ++i) {
        if(!this->getVertex(i)->boundary()) {
            _nodes.insertAlways(Node{this->getVertex(i)});
        }
    }
}

void FEMObject::_createRegularVertices(int n, int m, float r) {
    this->insertAlways(GMlib::TSVertex<float>(GMlib::Point<float, 2>(0.0f, 0.0f)));
    int elementsInRow = n;

    for(int j = 1; j <= m; ++j) {
        elementsInRow = n * j;
        for(int i = 0; i < elementsInRow; ++i) {
            double alpha = i * M_2PI / elementsInRow;
            SqMatrix<float, 2> R(alpha, Vector<float, 2>(1, 0),
                                        Vector<float, 2>(0, 1));
            Point<float, 2> p = R * Point<float, 2>(j * r / m, 0);
            this->insertAlways(TSVertex<float>(p));
        }
    }

    _boundaryPoints = elementsInRow;
    this->reverse();
}

void FEMObject::regularTriangulation(int n, int m, float r)
{
    _createRegularVertices(n, m, r);

    this->triangulateDelaunay();

    for(auto i = _boundaryPoints; i < this->getNoVertices(); ++i) {
        _nodes.insertAlways(Node{this->getVertex(i)});
    }
}

void FEMObject::randomTriangulation(int r, int m)
{
    int firstRowElements = 5;
    int rows = m / firstRowElements;
    int numberOfSwaps = m * m * m;

    _createRegularVertices(firstRowElements, rows, r);

    std::mt19937 rng;
    rng.seed(std::random_device()());
    std::uniform_int_distribution<std::mt19937::result_type> swapIndex(_boundaryPoints, this->getNoVertices() - 1);

    for(int i = 0; i < numberOfSwaps; ++i) {
        std::swap(*this->getVertex(swapIndex(rng)), *this->getVertex(swapIndex(rng)));
    }

    for(int i = 0; i < (this->getNoVertices() - _boundaryPoints) * 0.8; ++i) {
        this->removeBack();
    }

    this->triangulateDelaunay();

    for(auto i = _boundaryPoints; i < this->getNoVertices(); ++i) {
        _nodes.insertAlways(Node{this->getVertex(i)});
    }
}

void FEMObject::computation()
{

    // Compute elements of the stiffness matrix
    _A = DMatrix<float>(_nodes.size(), _nodes.size());

    for(int i = 0; i < _nodes.size(); ++i) {
        // Compute diagonal
        _A[i][i] = _computeElement(&_nodes[i]);

        // Compute non-diagonal
        for(int j = 0; j < i; ++j) {
            TSEdge<float>* edge = _nodes[i].getNeighbor(_nodes[j]);
            if(edge == nullptr) {
                _A[i][j] = _A[j][i] = 0.0f;
            }
            else {
                _A[i][j] = _A[j][i] = _computeElement(edge);
            }
        }
    }

    // Compute load vector
    _b = DVector<float>(_nodes.size());
    for(int i = 0; i < _nodes.size(); ++i) {
        _b[i] = _nodes[i].getArea2D();
    }

    // Compute weights
    _x = _A.invert() * _b;
}

void FEMObject::_updateHeight(float force)
{
    // Compute weights and update height
    auto x = _x * force;
    for(int i = 0; i < _nodes.size(); ++i) {
        _nodes[i].setZ(x[i]);
    }
}

void FEMObject::localSimulate(double dt)
{
    _f += dt * 2;
    _updateHeight(std::sin(_f));
}

Vector<Vector<float, 2>, 3> FEMObject::_findVectors(GMlib::TSEdge<float> *ed)
{
    auto tris = ed->getTriangle();

    auto p1 = ed->getFirstVertex();
    auto p2 = ed->getLastVertex();
    TSVertex<float>* p3 = nullptr;
    TSVertex<float>* p4 = nullptr;

    // Find p3 and p4
    auto tri1Vertices = tris[0]->getVertices();
    if(tri1Vertices[0] == p1)
        p3 = tri1Vertices[1] == p2 ? tri1Vertices[2] : tri1Vertices[1];
    else if(tri1Vertices[1] == p1)
        p3 = tri1Vertices[0] == p2 ? tri1Vertices[2] : tri1Vertices[0];
    else
        p3 = tri1Vertices[0] == p2 ? tri1Vertices[1] : tri1Vertices[0];

    auto tri2Vertices = tris[1]->getVertices();
    if(tri2Vertices[0] == p1)
        p4 = tri2Vertices[1] == p2 ? tri2Vertices[2] : tri2Vertices[1];
    else if(tri2Vertices[1] == p1)
        p4 = tri2Vertices[0] == p2 ? tri2Vertices[2] : tri2Vertices[0];
    else
        p4 = tri2Vertices[0] == p2 ? tri2Vertices[1] : tri2Vertices[0];

    Vector<Vector<float, 2>, 3> v;
    v[0] = p2->getParameter() - p1->getParameter();
    v[1] = p3->getParameter() - p1->getParameter();
    v[2] = p4->getParameter() - p1->getParameter();

    return v;
}

Vector<Vector<float, 2>, 3> FEMObject::_findVectors(GMlib::TSTriangle<float> *tr, Node *n)
{
    Array<TSVertex<float>*> verts = tr->getVertices();

    TSVertex<float>* p0 = nullptr;
    TSVertex<float>* p1 = nullptr;
    TSVertex<float>* p2 = nullptr;

    if(verts[0] == n->v()){
        p0 = verts[0];
        p1 = verts[1];
        p2 = verts[2];
    }
    else if(verts[1] == n->v()) {
        p0 = verts[1];
        p1 = verts[2];
        p2 = verts[0];
    }
    else {
        p0 = verts[2];
        p1 = verts[0];
        p2 = verts[1];
    }

    Vector<Vector<float, 2>, 3> v;
    v[0] = p1->getParameter() - p0->getParameter();
    v[1] = p2->getParameter() - p0->getParameter();
    v[2] = p2->getParameter() - p1->getParameter();

    return v;
}

float FEMObject::_computeElement(GMlib::TSEdge<float> *ed)
{
    auto v = _findVectors(ed);

    auto dd = 1 / (v[0] * v[0]);
    auto dh1 = dd * (v[1] * v[0]);
    auto dh2 = dd * (v[2] * v[0]);
    auto area1 = std::abs(v[0] ^ v[1]);
    auto area2 = std::abs(v[0] ^ v[2]);
    auto h1 = dd * static_cast<float>(std::pow(area1, 2));
    auto h2 = dd * static_cast<float>(std::pow(area2, 2));

    return ((dh1 * ((1 - dh1) / h1) - dd)) * (area1 / 2) + ((dh2 * ((1 - dh2)) / h2) - dd) * (area2 / 2);
}

float FEMObject::_computeElement(Node *n)
{
    float sum = 0;
    auto tris = n->getTriangles();

    for(int i = 0; i < tris.size(); ++i) {
        auto v = _findVectors(tris[i], n);
        sum += (v[2] * v[2]) / (2 * std::abs(v[0] ^ v[1]));
    }

    return sum;
}
