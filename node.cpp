#include "node.h"

using namespace GMlib;

Node::Node()
{

}

Node::Node(TSVertex<float>* v)
{
    _v = v;
}

Array<TSTriangle<float> *> Node::getTriangles()
{
    return _v->getTriangles();
}

TSEdge<float>* Node::getNeighbor(Node &n)
{
    auto edges = _v->getEdges();
    auto otherEdges = n.v()->getEdges();

    for(int i = 0; i < edges.size(); ++i) {
        for(int j = 0; j < otherEdges.size(); ++j) {
            if(edges[i] == otherEdges[j])
                return edges[i];
        }
    }

    return nullptr;
}

void Node::setZ(float z)
{
    _v->setZ(z);
}

TSVertex<float>* Node::v() {
    return _v;
}

float Node::getArea2D()
{
    auto tris = _v->getTriangles();
    float totalArea = 0;

    for(int i = 0; i < tris.size(); ++i) {
        totalArea += tris[i]->getArea2D();
    }

    return totalArea / 3.0f;
}
