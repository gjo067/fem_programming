#ifndef SCENARIO_H
#define SCENARIO_H


#include "application/gmlibwrapper.h"

// qt
#include <QObject>

namespace GMlib {
  template <typename T> class TriangleFacets;
}

class FEMObject;

class Scenario : public GMlibWrapper {
  Q_OBJECT
public:
  using GMlibWrapper::GMlibWrapper;

    void simulate();

  void    initializeScenario() override;
  void    cleanupScenario() override;

private:
  void initializeFEMObject();
//  void cleanupFEMObject();

  void initializeTriangleFacetExample();
  void cleanupTriangleFacetExample();

  void initializeTestTorusExample();
  void cleanupTestTorusExample();

  std::shared_ptr<TestTorus>                    m_test_torus{nullptr};
  std::shared_ptr<GMlib::TriangleFacets<float>> m_triangle_facets{nullptr};
  std::shared_ptr<FEMObject> _fem_regular{nullptr};
  std::shared_ptr<FEMObject> _fem_random{nullptr};
  std::shared_ptr<FEMObject> _fem_square{nullptr};
};

#endif // SCENARIO_H
