#ifndef NODE_H
#define NODE_H

#include <trianglesystem/gmtrianglesystem.h>

class Node
{
public:
    Node();
    Node(GMlib::TSVertex<float>* v);

    GMlib::Array<GMlib::TSTriangle<float>*> getTriangles();
    GMlib::TSEdge<float>* getNeighbor(Node& n);
    void setZ(float z);
    GMlib::TSVertex<float>* v();
    float getArea2D();

private:
    GMlib::TSVertex<float>* _v;

};

#endif // NODE_H
